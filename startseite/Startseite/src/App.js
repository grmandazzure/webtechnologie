import React from 'react';
import './App.css';
import Index from './components';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/index';
import Stundenplan from './pages/stundenplan';
import Onlinetraining from './pages/onlinetraining';
import Events from './pages/events';
import Logout from './pages/logout';

function App() {
return (
	<Router>
	<Index class="NavBar"/>
	<Routes >
		<Route path='/index' exact element={<Home/>} />
		<Route path='/pages/stundenplan' element={<Stundenplan/>} />
		<Route path='/onlinetraining' element={<Onlinetraining/>} />
		<Route path='/events' element={<Events/>} />
		<Route path='/logout' element={<Logout/>} />
	</Routes>
	</Router>
);
}

export default App;

