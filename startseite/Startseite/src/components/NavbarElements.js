import { NavLink as Link } from 'react-router-dom';
import styled from 'styled-components';

export const Nav = styled.nav`
background: F8F9FA;
height: 100px;
float: center;
display:inline-block;
padding: 0.2rem;
`;

export const NavLink = styled(Link)`
color: 212529;
text-decoration: none;
border: 3px solid gray;
cursor: pointer;
padding: 0.5rem;
padding-left: 8%;
padding-right: 8%;
&.active {
	color: F8F9FA;
	border: 3.5px solid red;
}
`;

export const NavMenu = styled.div`
position: center;
display: flex;
align-items: center;
justify-content: space-between;
padding: 10px;
color: white;
text-transform: capitalize;
width: 200%;
@media screen and (max-width: 800px) {
	display: none;
}
`;

export const NavBtn = styled.nav`
display: flex;
align-items: center;
margin-right: 24px;
@media screen and (max-width: 768px) {
	display: none;
}
`;

export const NavBtnLink = styled(Link)`
border-radius: 4px;
background: #808080;
padding: 10px 22px;
color: 212529;
outline: none;
border: none;
cursor: pointer;
transition: all 0.2s ease-in-out;
text-decoration: none;
margin-left: 24px;
&:hover {
	transition: all 0.2s ease-in-out;
	background: #fff;
	color: F8F9FA;
}
`;

export const WilkommenText = styled.nav`
padding-left: 180px;
`;