import React from "react"
import { WilkommenText } from './NavbarElements';
class Wilkommen extends React.Component {
  render() {
    return (
      <WilkommenText>
        <h1>Login erfolgreich</h1>
        Wilkommen im Mitgliederbereich. <br/>
		    Wählen Sie aus den gegebenen Trainingsoptionen aus:
      </WilkommenText>
    )
  }
}
export default Wilkommen