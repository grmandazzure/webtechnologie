import React from 'react';
import {
Nav,NavLink,NavMenu,NavBtn,NavBtnLink,
} from './NavbarElements';
import Wilkommen from './Wilkommen';



const Index = () => {
return (
	<>
	<Nav>
		<NavMenu>
		<img src ='/images/logo-removebg-preview.png' alt = "DogitalDojo"
	    width="150" height="150"/>
		<NavLink to='/pages/stundenplan' activeStyle>
			Stundenplan
		</NavLink>
		<NavLink to='/events' activeStyle>
			Events
		</NavLink>
		<NavLink to='/onlinetraining' activeStyle>
			Online Training
		</NavLink>
		<NavBtn>
		<NavBtnLink to='/logout'>Logout</NavBtnLink>
		</NavBtn>
		</NavMenu>
	</Nav>
	<Wilkommen />
	</>
);
};

export default Index;
